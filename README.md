apicore
======

Core library for building and testing Go APIs

## Overview

APIs are built using httprouter for improved performance. All http constructs are visible to allow compatibility with
other http utilities. This library provides utilities for common tasks and dependency injection using a controller
pattern.

## TODO More docs