package apicore

import (
	"fmt"
	"reflect"
)

type provider interface {
	provide() interface{}
}

type valueProvider struct {
	value interface{}
}

func (vp *valueProvider) provide() interface{} {
	return vp.value
}

type binder struct {
	bindings map[string]provider
}

func newBinder() *binder {
	return &binder {
		bindings: make(map[string]provider, 0),
	}
}

func (b *binder) bind(value interface{}) error {
	if value == nil {
		return fmt.Errorf("cannot bind a nil value")
	}

	t := reflect.TypeOf(value)

	if t.Kind() == reflect.Ptr {
		b.bindValue(value)
	} else {
		return fmt.Errorf("could not bind value %v", value)
	}

	return nil
}

func (b *binder) bindValue(value interface{}) error {
	t := reflect.TypeOf(value)
	key, err := typeKey(t)

	if err != nil {
		return err
	}

	t = t.Elem()

	 if t.Kind() == reflect.Interface {
		value = reflect.ValueOf(value).Elem().Interface()
	} else if t.Kind() != reflect.Struct {
		return fmt.Errorf("cannot bind a pointer that does not point to a struct or interface")
	}

	b.bindings[key] = &valueProvider{ value: value}
	return nil
}

func (b *binder) resolve(key string) interface{} {
	provider := b.bindings[key]
	if provider == nil {
		return nil
	} else {
		return provider.provide()
	}
}
