package unittest

import (
	"bitbucket.org/smproto/apicore"
	"github.com/stretchr/testify/require"
	"testing"
)

type testDep interface {
	Method() string
}

type testImpl struct {
	val string
}

func (t *testImpl) Method() string {
	return t.val
}

// Interface dependence

type testInterfaceDependent struct {
	Dep testDep `apicore:"inject"`
}

func TestDi_InjectsInterface(t *testing.T) {
	var dep testDep = &testImpl{ val: "test" }
	dependent := new(testInterfaceDependent)
	context := apicore.NewContext()

	context.Bind(&dep)
	context.AddDependents(dependent)

	require.NoError(t, context.Install())
	require.NotNil(t, dependent.Dep)
	require.Equal(t, "test", dependent.Dep.Method())
}

// Struct Dependence

type testStructDependent struct {
	Dep *testImpl `apicore:"inject"`
}

func TestDi_InjectsPointerToStruct(t *testing.T) {
	dep := &testImpl{ val: "test" }
	dependent := new(testStructDependent)
	context := apicore.NewContext()

	context.Bind(dep)
	context.AddDependents(dependent)

	require.NoError(t, context.Install())
	require.NotNil(t, dependent.Dep)
	require.Equal(t, "test", dependent.Dep.Method())
}

// Other binding

type testNonTaggedDependent struct {
	Dep testDep
}

func TestDi_NotRegisteredReturnsError(t *testing.T) {
	dependent := new(testInterfaceDependent)
	context := apicore.NewContext()

	context.AddDependents(dependent)

	require.Error(t, context.Install())
	require.Nil(t, dependent.Dep)
}

func TestDi_IgnoresUntaggedWhenTypeIsBound(t *testing.T) {
	var dep testDep = &testImpl{ val: "test" }
	dependent := new(testNonTaggedDependent)
	context := apicore.NewContext()

	context.Bind(&dep)
	context.AddDependents(dependent)

	require.NoError(t, context.Install())
	require.Nil(t, dependent.Dep)
}

type testNestedDep struct {
	Dep *testInterfaceDependent `apicore:"inject"`
}

func TestDi_DependenciesOfDependenciesAreBound(t *testing.T) {
	var dep testDep = &testImpl{ val: "test" }
	parent := new(testInterfaceDependent)
	child := new(testNestedDep)
	context := apicore.NewContext()

	context.Bind(&dep)
	context.Bind(parent)
	context.AddDependents(child)

	require.NoError(t, context.Install())
	require.NotNil(t, child.Dep)
	require.NotNil(t, child.Dep.Dep)
	require.Equal(t, "test", child.Dep.Dep.Method())
}

// Circular Dependencies

type circular1 struct {
	C2 *circular2 `apicore:"inject"`
}

type circular2 struct {
	C1 *circular1 `apicore:"inject"`
}

func TestDi_CircularDependencyReturnsError(t *testing.T) {
	c1 := new(circular1)
	c2 := new(circular2)
	context := apicore.NewContext()

	context.Bind(c1)
	context.Bind(c2)
	context.AddDependents(c1)
	context.AddDependents(c2)

	require.Error(t, context.Install())
}

// Inheritance

func TestDi_ChildBindingsPrioritized(t *testing.T) {
	var dep1 testDep = &testImpl{ val: "test1" }
	var dep2 testDep = &testImpl{ val: "test2" }

	dependent := new(testInterfaceDependent)
	parent := apicore.NewContext()
	child := parent.SubContext("")

	parent.Bind(&dep1)
	child.Bind(&dep2)
	child.AddDependents(dependent)

	require.NoError(t, parent.Install())
	require.NotNil(t, dependent.Dep)
	require.Equal(t, "test2", dependent.Dep.Method())
}

func TestDi_ParentBindingsInherited(t *testing.T) {
	var dep testDep = &testImpl{ val: "test" }

	dependent := new(testInterfaceDependent)
	parent := apicore.NewContext()
	child := parent.SubContext("")

	parent.Bind(&dep)
	child.AddDependents(dependent)

	require.NoError(t, parent.Install())
	require.NotNil(t, dependent.Dep)
	require.Equal(t, "test", dependent.Dep.Method())
}