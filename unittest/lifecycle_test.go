package unittest

import (
	"bitbucket.org/smproto/apicore"
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

type testOnInstall struct {
	err error
}

func (t *testOnInstall) OnInstall() error {
	return t.err
}

func TestOnInstall_RunOnDependent(t *testing.T) {
	expected := fmt.Errorf("test")
	handler := &testOnInstall{ err: expected }
	context := apicore.NewContext()

	context.AddDependents(handler)
	require.Equal(t, expected, context.Install())
}

type testOnInstallDependent struct {
	Dep *testOnInstall `apicore:"inject"`
}

func TestOnInstall_RunOnInjectedDependency(t *testing.T) {
	expected := fmt.Errorf("test")
	handler := &testOnInstall{ err: expected }
	dependent := new(testOnInstallDependent)
	context := apicore.NewContext()

	context.Bind(handler)
	context.AddDependents(dependent)
	require.Equal(t, expected, context.Install())
}

// Inheritance

func TestOnInstall_RunOnChildContextDependent(t *testing.T) {
	expected := fmt.Errorf("test")
	handler := &testOnInstall{ err: expected }
	parent := apicore.NewContext()
	child := parent.SubContext("")

	child.AddDependents(handler)
	require.Equal(t, expected, parent.Install())
}

func TestOnInstall_RunOnChildContextInjectedDependency(t *testing.T) {
	expected := fmt.Errorf("test")
	handler := &testOnInstall{ err: expected }
	dependent := new(testOnInstallDependent)
	parent := apicore.NewContext()
	child := parent.SubContext("")

	child.Bind(handler)
	child.AddDependents(dependent)
	require.Equal(t, expected, parent.Install())
}
