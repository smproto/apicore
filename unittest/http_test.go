package unittest

import (
	"bitbucket.org/smproto/apicore"
	"github.com/julienschmidt/httprouter"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

type testController struct {}

func (c *testController) Execute(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	w.WriteHeader(http.StatusOK)
}

func TestHttpController_Get(t *testing.T) {
	req := httptest.NewRequest("GET", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	context := apicore.NewContext()
	context.Get(req.URL.Path, ctl)

	router, err := context.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}

func TestHttpController_Post(t *testing.T) {
	req := httptest.NewRequest("POST", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	context := apicore.NewContext()
	context.Post(req.URL.Path, ctl)

	router, err := context.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}

func TestHttpController_Put(t *testing.T) {
	req := httptest.NewRequest("PUT", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	context := apicore.NewContext()
	context.Put(req.URL.Path, ctl)

	router, err := context.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}

func TestHttpController_Delete(t *testing.T) {
	req := httptest.NewRequest("DELETE", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	context := apicore.NewContext()
	context.Delete(req.URL.Path, ctl)

	router, err := context.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}

// Inheritance

func TestHttpController_ChildContextGet(t *testing.T) {
	req := httptest.NewRequest("GET", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	parent := apicore.NewContext()
	child := parent.SubContext("")
	child.Get(req.URL.Path, ctl)

	router, err := parent.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}

func TestHttpController_ChildContextPost(t *testing.T) {
	req := httptest.NewRequest("POST", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	parent := apicore.NewContext()
	child := parent.SubContext("")
	child.Post(req.URL.Path, ctl)

	router, err := parent.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}

func TestHttpController_ChildContextPut(t *testing.T) {
	req := httptest.NewRequest("PUT", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	parent := apicore.NewContext()
	child := parent.SubContext("")
	child.Put(req.URL.Path, ctl)

	router, err := parent.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}

func TestHttpController_ChildContextDelete(t *testing.T) {
	req := httptest.NewRequest("DELETE", "/test", nil)
	w := httptest.NewRecorder()

	ctl := new(testController)
	parent := apicore.NewContext()
	child := parent.SubContext("")
	child.Delete(req.URL.Path, ctl)

	router, err := parent.GetHttpRouter()
	require.NoError(t, err)
	router.ServeHTTP(w, req)

	require.Equal(t, http.StatusOK, w.Code)
}