package unittest

import (
	"bitbucket.org/smproto/apicore"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestLoadRequestJson_Success(t *testing.T) {
	b, _ := json.Marshal(map[string]interface{}{
		"test": "value",
	})

	req := httptest.NewRequest("POST", "/test", bytes.NewReader(b))
	req.Header.Set("Content-Type", "application/json")
	target := make(map[string]interface{}, 0)

	require.NoError(t, apicore.LoadRequestJson(req, &target))
	require.Equal(t, "value", target["test"])
}

func TestLoadRequestJson_NoContentTypeHeader(t *testing.T) {
	b, _ := json.Marshal(map[string]interface{}{
		"test": 1,
	})

	req := httptest.NewRequest("POST", "/test", bytes.NewReader(b))
	target := make(map[string]interface{}, 0)

	require.Error(t, apicore.LoadRequestJson(req, &target))
}

func TestLoadRequestJson_InvalidContentTypeHeader(t *testing.T) {
	b, _ := json.Marshal(map[string]interface{}{
		"test": 1,
	})

	req := httptest.NewRequest("POST", "/test", bytes.NewReader(b))
	req.Header.Set("Content-Type", "text/html")
	target := make(map[string]interface{}, 0)

	require.Error(t, apicore.LoadRequestJson(req, &target))
}

func TestLoadRequestJson_BadJson(t *testing.T) {
	req := httptest.NewRequest("POST", "/test", bytes.NewReader([]byte("}{")))
	req.Header.Set("Content-Type", "application/json")
	target := make(map[string]interface{}, 0)

	require.Error(t, apicore.LoadRequestJson(req, &target))
}

func TestRespondJson_Success(t *testing.T) {
	body := map[string]interface{}{
		"test": "value",
	}

	w := httptest.NewRecorder()
	require.NoError(t, apicore.RespondJson(w, http.StatusOK, body))

	require.Equal(t, http.StatusOK, w.Code)

	target := make(map[string]interface{}, 0)
	require.NoError(t, json.NewDecoder(w.Body).Decode(&target))
	require.Equal(t, body["test"], target["test"])
}

func TestRespondSring_NoFormat(t *testing.T) {
	w := httptest.NewRecorder()
	apicore.RespondString(w, http.StatusOK, "test")

	require.Equal(t, http.StatusOK, w.Code)
	s, err := ioutil.ReadAll(w.Body)
	require.NoError(t, err)
	require.Equal(t, "test", string(s))
}

func TestRespondString_WithFormat(t *testing.T) {
	w := httptest.NewRecorder()
	apicore.RespondString(w, http.StatusOK, "%s", "test")

	require.Equal(t, http.StatusOK, w.Code)
	s, err := ioutil.ReadAll(w.Body)
	require.NoError(t, err)
	require.Equal(t, "test", string(s))
}

func TestRespondInternalError_Success(t *testing.T) {
	w := httptest.NewRecorder()
	expected := fmt.Errorf("test")

	apicore.RespondInternalError(w, expected)
	require.Equal(t, http.StatusInternalServerError, w.Code)
	s, err := ioutil.ReadAll(w.Body)
	require.NoError(t, err)
	require.True(t, strings.Contains(string(s), expected.Error()))
}