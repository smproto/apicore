package unittest

import (
	"bitbucket.org/smproto/apicore"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"os"
	"testing"
)

func TestConfig_LoadFromJson(t *testing.T) {
	b, _ := json.Marshal(map[string]string {
		"test": "value",
	})

	f, err := ioutil.TempFile("", "test.json")
	f.Write(b)
	if err != nil {
		panic(err)
	}

	defer os.Remove(f.Name())
	context := apicore.NewContext()

	err = context.LoadJsonConfig(f.Name())
	require.NoError(t, err)
	config := context.RawConfig()
	require.Equal(t, "value", config["test"])
}

func TestConfig_LoadFromMap(t *testing.T) {
	testConfig := map[string]interface{} {
		"test": "value",
	}

	context := apicore.NewContext()
	context.LoadMapConfig(testConfig)
	config := context.RawConfig()

	require.Equal(t, testConfig["test"], config["test"])
}

func TestConfigInjection_RootConfig(t *testing.T) {
	dependent := &struct {
		Config *struct {
			Test string
		} `apicore:"config"`
	}{}

	testConfig := map[string]interface{} {
		"test": "value",
	}

	context := apicore.NewContext()
	context.LoadMapConfig(testConfig)

	context.AddDependents(dependent)
	require.NoError(t, context.Install())
	require.Equal(t, "value", dependent.Config.Test)
}

func TestConfigInjection_SingleLevelNestedConfig(t *testing.T) {
	dependent := &struct {
		Config *struct {
			Test string
		} `apicore:"config" root:"nest"`
	}{}

	testConfig := map[string]interface{} {
		"nest": map[string]interface{}{
			"test": "value",
		},
	}

	context := apicore.NewContext()
	context.LoadMapConfig(testConfig)

	context.AddDependents(dependent)
	require.NoError(t, context.Install())
	require.Equal(t, "value", dependent.Config.Test)
}

func TestConfigInjection_MultiLevelNestedConfig(t *testing.T) {
	dependent := &struct {
		Config *struct {
			Test string
		} `apicore:"config" root:"nest1.nest2"`
	}{}

	testConfig := map[string]interface{} {
		"nest1": map[string]interface{}{
			"nest2": map[string]interface{}{
				"test": "value",
			},
		},
	}

	context := apicore.NewContext()
	context.LoadMapConfig(testConfig)

	context.AddDependents(dependent)
	require.NoError(t, context.Install())
	require.Equal(t, "value", dependent.Config.Test)
}

func TestConfigInjection_NonPointerTarget(t *testing.T) {
	dependent := struct {
		Config struct {
			Test string
		} `apicore:"config"`
	}{}

	testConfig := map[string]interface{} {
		"test": "value",
	}

	context := apicore.NewContext()
	context.LoadMapConfig(testConfig)

	context.AddDependents(dependent)
	require.Error(t, context.Install())
}

// Inheritance

func TestConfigInjection_ChildConfigRoot(t *testing.T) {
	dependent := &struct {
		Config *struct {
			Test string
		} `apicore:"config"`
	}{}

	testConfig := map[string]interface{} {
		"nest": map[string]interface{}{
			"test": "value",
		},
	}

	parent := apicore.NewContext()
	parent.LoadMapConfig(testConfig)

	child := parent.SubContext("nest")
	child.AddDependents(dependent)

	require.NoError(t, parent.Install())
	require.Equal(t, "value", dependent.Config.Test)
}

func TestConfigInjection_NestedConfigInChildContext(t *testing.T) {
	dependent := &struct {
		Config *struct {
			Test string
		} `apicore:"config" root:"nest2"`
	}{}

	testConfig := map[string]interface{} {
		"nest1": map[string]interface{}{
			"nest2": map[string]interface{}{
				"test": "value",
			},
		},
	}

	parent := apicore.NewContext()
	parent.LoadMapConfig(testConfig)

	child := parent.SubContext("nest1")
	child.AddDependents(dependent)

	require.NoError(t, parent.Install())
	require.Equal(t, "value", dependent.Config.Test)
}

func TestConfigInjection_MultiLevelChildConfigRoot(t *testing.T) {
	dependent := &struct {
		Config *struct {
			Test string
		} `apicore:"config"`
	}{}

	testConfig := map[string]interface{} {
		"nest1": map[string]interface{}{
			"nest2": map[string]interface{}{
				"test": "value",
			},
		},
	}

	parent := apicore.NewContext()
	parent.LoadMapConfig(testConfig)

	child := parent.SubContext("nest1.nest2")
	child.AddDependents(dependent)

	require.NoError(t, parent.Install())
	require.Equal(t, "value", dependent.Config.Test)
}