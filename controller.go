package apicore

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

// Controllers are structures which declare dependencies and execute a single HTTP call. When the core is started, all
// dependencies will be resolved and this controller will be connected to an httprouter
type HttpController interface {
	Execute(w http.ResponseWriter, req *http.Request, params httprouter.Params)
}
