package apicore

import (
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"os"
	"reflect"
)

// The core is the central place to declare dependencies and register controllers. Dependencies can be registered
// using the Bind method, while controllers can be configured to listen to traffic using any of the http methods
// defined (Get, Post, etc.)
//
// Cores work with two essential units of work: Controllers and Services. Services execute work on behalf of the
// application, usually interacting with external resources like file systems and databases. Controllers perform
// the core work of the application and control http resources like requests and responses.
//
// Both types of units can declare two types of dependencies: Injections and Configuration. If a dependency cannot
// be resolved or a circular dependency is found, the application will fail unless that behavior is disabled
//
// Start listening to traffic using the Start method and providing a host and port to listen on. Simply provide an
// empty string to ignore the host and provide a port only
type Context interface {
	Bind(values... interface{}) error

	Get(path string, ctl HttpController)
	Post(path string, ctl HttpController)
	Put(path string, ctl HttpController)
	Delete(path string, ctl HttpController)

	AddDependents(targets... interface{})

	SubContext(configRoot string) Context
}

type RootContext interface {
	Context

	LoadJsonConfig(paths... string) error
	LoadMapConfig(config map[string]interface{})
	RawConfig() Config

	Install() error
	GetHttpRouter() (*httprouter.Router, error)
}

func NewContext() RootContext {
	return newContext(nil, "")
}

func newContext(parent *context, configRoot string) *context {
	if parent != nil && parent.configRoot != "" {
		configRoot = fmt.Sprintf("%s.%s", parent.configRoot, configRoot)
	}

	var config *configManager
	var deps *dependencyManager

	if parent == nil {
		config = newConfigManager()
		deps = newDepManager()
	} else {
		config = parent.config
		deps = parent.deps
	}

	return &context {
		configRoot: configRoot,
		parent: parent,
		children: make([]*context, 0),
		dependents: make([]interface{}, 0),

		putControllers: make(map[string]HttpController, 0),
		postControllers: make(map[string]HttpController, 0),
		deleteControllers: make(map[string]HttpController, 0),
		getControllers: make(map[string]HttpController, 0),

		config: config,
		binder: newBinder(),
		deps: deps,
	}
}

type context struct {
	parent *context
	children []*context

	config *configManager
	configRoot string

	binder *binder
	deps *dependencyManager

	dependents []interface{}

	getControllers map[string]HttpController
	putControllers map[string]HttpController
	postControllers map[string]HttpController
	deleteControllers map[string]HttpController
}

func (c *context) SubContext(configRoot string) Context {
	child := newContext(c, configRoot)
	c.children = append(c.children, child)
	return child
}

func (c *context) Bind(values... interface{}) error {
	for _, value := range values {
		if err := c.binder.bind(value); err != nil {
			return err
		}
	}
	return nil
}

func (c *context) LoadJsonConfig(paths ... string) error {
	for _, path := range paths {
		var config Config

		if f, err := os.Open(path); err != nil {
			return err
		} else if err = json.NewDecoder(f).Decode(&config); err != nil {
			return err
		} else {
			c.LoadMapConfig(config)
		}
	}

	return nil
}

func (c *context) LoadMapConfig(config map[string]interface{}) {
	c.config.addConfig(config)
}

func (c *context) RawConfig() Config {
	return c.config.rawConfig
}

func (c *context) Get(path string, ctl HttpController) {
	c.getControllers[path] = ctl
	c.AddDependents(ctl)
}

func (c *context) Post(path string, ctl HttpController) {
	c.postControllers[path] = ctl
	c.AddDependents(ctl)
}

func (c *context) Put(path string, ctl HttpController) {
	c.putControllers[path] = ctl
	c.AddDependents(ctl)
}

func (c *context) Delete(path string, ctl HttpController) {
	c.deleteControllers[path] = ctl
	c.AddDependents(ctl)
}

func (c *context) AddDependents(targets... interface{}) {
	c.dependents = append(c.dependents, targets...)
}

func (c *context) Install() error {
	for _, dependent := range c.dependents {
		if err := c.install(dependent); err != nil {
			return err
		}
	}

	for _, child := range c.children {
		if err := child.Install(); err != nil {
			return err
		}
	}

	return nil
}

func (c *context) install(target interface{}) error {
	t, v, err := validateInstallTarget(target)
	if err != nil {
		return err
	}

	targetKey, _ := typeKey(reflect.TypeOf(target))

	for i := 0; i < v.NumField(); i++ {
		tag := t.Field(i).Tag.Get("apicore")
		if tag == "" {
			continue
		}

		vField := v.Field(i)
		if !vField.CanSet() || (vField.Kind() != reflect.Ptr && vField.Kind() != reflect.Interface) {
			return fmt.Errorf("cannot assign to %s.%s (field must be an exported pointer or interface", t.Name(), t.Field(i).Name)

		} else if tag == "inject" {
			if key, err := typeKey(t.Field(i).Type); err != nil {
				return err
			} else if val, err := c.resolve(targetKey, key); err != nil {
				return err
			} else {
				vField.Set(reflect.ValueOf(val))
			}

		} else if tag == "config" {
			configRoot := c.configRoot
			tagRoot := t.Field(i).Tag.Get("root")

			// Separate the roots by a dot if both roots are defined
			if c.configRoot == "" || tagRoot == "" {
				configRoot += tagRoot
			} else {
				configRoot = fmt.Sprintf("%s.%s", c.configRoot, tagRoot)
			}

			if vField.IsNil() {
				vField.Set(reflect.New(t.Field(i).Type.Elem()))
			}
			if err := c.config.mapConfigTo(configRoot, vField.Interface()); err != nil {
				return err
			}

		} else {
			return fmt.Errorf("unrecognized apicore tag value on %s.%s (%s)", t.Elem().Name(), t.Field(i).Name, tag)
		}
	}

	if onInstall, ok := target.(OnInstallLifecycle); ok {
		return onInstall.OnInstall()
	}

	return nil
}

func (c *context) resolve(targetKey string, valKey string) (interface{}, error) {
	val := c.binder.resolve(valKey)

	if val == nil {
		if c.parent == nil {
			return nil, fmt.Errorf("type %s is not bound", valKey)
		} else {
			return c.parent.resolve(targetKey, valKey)
		}
	} else if err := c.deps.mapDep(targetKey, valKey); err != nil {
		return nil, err
	} else if err := c.install(val); err != nil {
		return nil, err
	}

	return val, nil
}

func (c *context) GetHttpRouter() (*httprouter.Router, error) {
	err := c.Install()
	if err != nil {
		return nil, err
	}

	r := httprouter.New()
	c.installControllersIntoRouter(r)

	return r, nil
}

func (c *context) installControllersIntoRouter(r *httprouter.Router) {
	for path, ctl := range c.getControllers {
		r.GET(path, ctl.Execute)
	}
	for path, ctl := range c.postControllers {
		r.POST(path, ctl.Execute)
	}
	for path, ctl := range c.putControllers {
		r.PUT(path, ctl.Execute)
	}
	for path, ctl := range c.deleteControllers {
		r.DELETE(path, ctl.Execute)
	}

	for _, child := range c.children {
		child.installControllersIntoRouter(r)
	}
}
