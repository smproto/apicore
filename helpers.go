package apicore

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func LoadRequestJson(req *http.Request, target interface{}) error {
	 if header := req.Header.Get("Content-Type"); header != "application/json" {
		return fmt.Errorf("invalid content type header (%s) provided", header)

	} else if err := json.NewDecoder(req.Body).Decode(target); err != nil {
		return err

	} else {
		return nil
	 }
}

func RespondJson(w http.ResponseWriter, code int, body interface{}) error {
	if err := json.NewEncoder(w).Encode(body); err != nil {
		return err
	} else {
		w.WriteHeader(code)
		return nil
	}
}

func RespondString(w http.ResponseWriter, code int, format string, params... interface{}) {
	w.WriteHeader(code)
	w.Write([]byte(fmt.Sprintf(format, params...)))
}

func RespondInternalError(w http.ResponseWriter, err error) {
	RespondString(w, http.StatusInternalServerError, "Internal server error encountered: %v", err)
}