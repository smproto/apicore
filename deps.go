package apicore

import "fmt"

type dependencyManager struct {
	depMap map[string][]string
}

func newDepManager() *dependencyManager {
	return &dependencyManager{
		depMap: make(map[string][]string, 0),
	}
}

func (dm *dependencyManager) mapDep(source string, dep string) error {
	// Check in the dependency's dependencies for circular dependency
	depDeps := dm.depMap[dep]
	if depDeps != nil {
		for _, depDep := range depDeps {
			if depDep == source {
				return fmt.Errorf("circular dependency found: %s <-> %s", source, dep)
			}
		}
	}

	// Check to see if the value has already been mapped successfully
	deps := dm.depMap[source]
	if deps == nil {
		dm.depMap[source] = []string { dep }
	} else {
		for _, d := range deps {
			if d == dep {
				return nil
			}
		}
		dm.depMap[source] = append(dm.depMap[source], dep)
	}

	return nil
}