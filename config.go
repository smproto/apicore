package apicore

import (
	"fmt"
	"github.com/mitchellh/mapstructure"
	"strings"
)

type Config map[string]interface{}

type configManager struct {
	rawConfig Config
}

func newConfigManager() *configManager {
	return &configManager{
		rawConfig: make(Config, 0),
	}
}

func (cm *configManager) addConfig(config Config) {
	for k, v := range config {
		cm.rawConfig[k] = v
	}
}

func (cm *configManager) mapConfigTo(configRoot string, target interface{}) (err error) {
	if c, err := cm.getConfigAt(configRoot); err != nil {
		return err
	} else {
		return mapstructure.Decode(c, target)
	}
}

func (cm *configManager) getConfigAt(configRoot string) (Config, error) {
	if configRoot == "" {
		return cm.rawConfig, nil
	}

	segments := strings.Split(configRoot, ".")
	config := cm.rawConfig

	for i, seg := range segments {
		if cm, ok := config[seg].(map[string]interface{}); ok {
			config = cm
		} else {
			return Config{}, fmt.Errorf("cannot map %s (%s is not an object)", configRoot, strings.Join(segments[0:i+1], "."))
		}
	}

	return config, nil
}