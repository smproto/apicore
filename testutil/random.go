package testutil

import (
	"fmt"
	"github.com/google/uuid"
	"strings"
)

func RandomString(length int) string {
	s := ""
	for (length >= 0 && len(s) < length) || (length < 0 && len(s) == 0) {
		s += strings.Replace(uuid.New().String(), "-", "", -1)
	}

	if length < 0 {
		return s
	} else {
		return s[0:length]
	}
}

func RandomEmail() string {
	return fmt.Sprintf("%s@domain.com", RandomString(5))
}
