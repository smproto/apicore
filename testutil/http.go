package testutil

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
)

func NewJsonRequest(method string, body interface{}) *http.Request {
	b, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}

	req := httptest.NewRequest(method, "/test", bytes.NewReader(b))
	req.Header.Set("Content-Type", "application/json")
	return req
}

func LoadResponseString(w *httptest.ResponseRecorder) string {
	if b, err := ioutil.ReadAll(w.Body); err != nil {
		panic(err)
	} else {
		return string(b)
	}
}

func LoadResponseJson(w *httptest.ResponseRecorder, target interface{}) {
	if err := json.NewDecoder(w.Body).Decode(&target); err != nil {
		panic(err)
	}
}