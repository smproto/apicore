module bitbucket.org/smproto/apicore

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.0.0 // indirect
	github.com/julienschmidt/httprouter v1.2.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.2.2
)
