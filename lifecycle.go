package apicore


type OnInstallLifecycle interface {
	OnInstall() error
}
