package apicore

import (
	"fmt"
	"reflect"
)

func typeKey(t reflect.Type) (string, error) {
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	if t.Kind() == reflect.Struct || t.Kind() == reflect.Interface {
		return fmt.Sprintf("%s.%s", t.PkgPath(), t.Name()), nil
	}

	return "", fmt.Errorf("bound pointer does not point to a struct or interface")
}

func validateInstallTarget(target interface{}) (reflect.Type, reflect.Value, error) {
	t := reflect.TypeOf(target)
	if t.Kind() != reflect.Ptr {
		return nil, reflect.Value{}, fmt.Errorf("cannot install dependencies into non-pointer target")
	}

	v := reflect.ValueOf(target)
	for t.Kind() == reflect.Ptr {
		v = v.Elem()
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		return nil, reflect.Value{}, fmt.Errorf("cannot install install dependencies into pointer to non-struct value")
	}

	return t, v, nil
}
